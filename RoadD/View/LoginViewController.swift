//
//  LoginViewController.swift
//  RoadD
//
//  Created by Khanathip Rachprakhon  on 9/5/2567 BE.
//

import UIKit
import FirebaseCore
import FirebaseAuth

class LoginViewController: UIViewController {

    
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    
    
    var handle: AuthStateDidChangeListenerHandle?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true

        
        self.usernameTextField.text = "khanathip.ra@gmail.com"
        self.passwordTextField.text = "0811861160"
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.loginButton.layer.cornerRadius = 12
        self.passwordTextField.isSecureTextEntry = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handle = Auth.auth().addStateDidChangeListener { auth, user in
          
            
            print("addStateDidChangeListener auth : \(auth) | user : \(user) ")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        guard let email = usernameTextField.text, let password = passwordTextField.text else { return }

        print("loginAction")
        
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
          guard let strongSelf = self else { return }
            

            if let error = error {
                self?.showAlert(title: "Error", message: error.localizedDescription)
            } else {
                if let authResult = authResult {

//                    print("addStateDidChangeListener uid : \(authResult.user.uid) | providerID : \(authResult.user.providerID) ")
//                    print("addStateDidChangeListener displayName : \(authResult.user.displayName) | photoURL : \(authResult.user.photoURL) ")
//                    print("addStateDidChangeListener email : \(authResult.user.email) | phoneNumber : \(authResult.user.phoneNumber) ")

                    self?.navigationController?.popViewController(animated: false)

                } else {
                    self?.showAlert(title: "Error", message: "An unknown error occurred.")
                }
            }

        }
    }
    @IBAction func forgotAction(_ sender: Any) {
        print("forgotAction")

    }
    @IBAction func registerAction(_ sender: Any) {
        let registerViewController = RegisterViewController()
        self.navigationController?.pushViewController(registerViewController, animated: false)

    }
    
    func showAlert(title: String, message: String) {

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

}
