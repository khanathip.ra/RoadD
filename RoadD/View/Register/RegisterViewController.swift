//
//  RegisterViewController.swift
//  RoadD
//
//  Created by Khanathip Rachprakhon  on 20/5/2567 BE.
//

import UIKit
import FirebaseAuth

class RegisterViewController: BaseViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var signupButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.title = "Register"
    }
    
    
    @IBAction func signupAction(_ sender: Any) {
        
        self.didSignUp()
        
    }
    
    
    func didSignUp(){
        guard let email = usernameTextField.text, let password = passwordTextField.text else { return }
        
        print("registerAction")
        
        Auth.auth().createUser(withEmail: email , password: password) { authResult, error in
          
            print("addStateDidChangeListener authResult : \(authResult) | error : \(error) ")

            if let error = error {
                self.showAlert(title: "Error", message: error.localizedDescription)
            } else {
                if let authResult = authResult {
                    self.showAlert(title: "Success", message: "User created with ID: \(authResult.user.uid)")
                } else {
                    self.showAlert(title: "Error", message: "An unknown error occurred.")
                }
            }

            
        }
    }

}
