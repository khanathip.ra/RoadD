//
//  MainTabbarViewController.swift
//  RoadD
//
//  Created by Khanathip Rachprakhon  on 24/4/2567 BE.
//

import UIKit

class MainTabbarViewController: UITabBarController{
    
    override func viewDidLoad() {
            super.viewDidLoad()
            
        tabBar.backgroundColor = .brown
        
        // Create view controllers for each tab
        let mainViewController = MainViewController()
        mainViewController.view.backgroundColor = .red
        mainViewController.title = "First"
        
        let secondViewController = UIViewController()
        secondViewController.view.backgroundColor = .blue
        secondViewController.title = "Second"
        
        let thirdViewController = UIViewController()
        thirdViewController.view.backgroundColor = .green
        thirdViewController.title = "Third"
        
        let fourthViewController = UIViewController()
        fourthViewController.view.backgroundColor = .yellow
        fourthViewController.title = "Fourth"
        
        let fifthViewController = UIViewController()
        fifthViewController.view.backgroundColor = .purple
        fifthViewController.title = "Fifth"
        
        // Set view controllers for the tab bar controller
        self.viewControllers = [mainViewController, secondViewController, thirdViewController, fourthViewController, fifthViewController]
    }
}
