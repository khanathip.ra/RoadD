//
//  ViewController.swift
//  RoadD
//
//  Created by Khanathip Rachprakhon  on 24/4/2567 BE.
//

import UIKit

class MainViewController: UIViewController {
    
       override func viewDidLoad() {
           super.viewDidLoad()


           
           
           
           let otherViewController = UIViewController() // Assuming OtherViewController is your other view controller
           otherViewController.view.backgroundColor = .gray
           otherViewController.title = "Second"

           let secondViewController = UIViewController()
           secondViewController.view.backgroundColor = .blue
           secondViewController.title = "Second"
           
           let thirdViewController = UIViewController()
           thirdViewController.view.backgroundColor = .green
           thirdViewController.title = "Third"

           let tabBarController = UITabBarController()
           
           tabBarController.viewControllers = [otherViewController, secondViewController, thirdViewController]
           
           tabBarController.selectedIndex = 0
           tabBarController.tabBar.backgroundColor = .brown

           navigationController?.pushViewController(tabBarController, animated: true)

           
           let storyboard = UIStoryboard(name: "LoginView", bundle: nil)
           if let loginViewController = storyboard.instantiateInitialViewController() {

               self.navigationController?.pushViewController(loginViewController, animated: true)
           }

       }
   }
